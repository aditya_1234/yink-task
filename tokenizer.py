import sys

Symbol = str        
List   = list         
Number = (int, float)

################ Parsing

def parse(expression):
    '''
    Parse the expression
    program-->str
    return -->list of tokens
    '''
    return token_read(tokenize(expression))

def tokenize(s):
    '''
    Convert a string into a list of tokens.
    s-->str
    return-->list
    '''
    return s.replace('(', ' ( ').replace(')', ' ) ').split()

def token_read(tokens):
    '''
    Create a list for tokens
    tokens-->list, each element is a str
    return-->list, each element may be int,list,str
    '''
    if len(tokens) == 0:
        print('Invalid program')
        #raise SyntaxError('unexpected EOF while reading')
        sys.exit()
    token = tokens.pop(0)
    if token == '(':
        L = []
        while tokens[0] != ')':
            L.append(token_read(tokens))
        tokens.pop(0) # pop off ')'
        return L
    elif token == ')':
        print('Invalid program')
        #raise SyntaxError('unexpected )')
        sys.exit()
    else:
        return atom(token)

def atom(token):
    '''
    Numbers become numbers; every other token is a symbol.
    token-->str
    return-->int,float,str depending on token
    Checks the validity of the variable
    '''
    try: return int(token)
    except ValueError:
        try: return float(token)
        except ValueError:
            val = Symbol(token)
            if val in '+-/*':
                return val
            for i in val:
                if i not in 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ':   #checking variable name is valid i.e. in a-z,A-Z
                    print('Invalid program')
                    #raise SyntaxError('Invalid variable name')
                    sys.exit()
            return Symbol(token)

