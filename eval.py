import tokenizer,sys

BIND = 'bind'

################ Environments

def standard_env():
    '''
    Create a dictionary with operators
    '''
    import operator as op
    env = Env()
    env.update({
        '+':op.add, '-':op.sub, '*':op.mul, '/':op.div, 
    })
    return env

class Env(dict):
    '''
    An environment: a dict of {'var':val} pairs, with an outer Env.
    '''
    def __init__(self, parms=(), args=(), outer=None):
        self.update(zip(parms, args))
        self.outer = outer
    def find(self, var):
        '''
        Find the innermost Env where var appears.
        '''
        return self if (var in self) else self.outer.find(var)
    
global_env = standard_env()

def eval(x, f=0,env=global_env):
    '''
    Evaluate an expression in an environment
    (f is set to 1 when the last expression is bind to print the value)
    '''
    if(isinstance(x,tokenizer.List) and len(x)==0):
        print('Invalid Program')
        sys.exit()
    if isinstance(x, tokenizer.Symbol):
        try:
            return env.find(x)[x]
        except:
            print('Invalid Program')
            #raise SyntaxError('Invalid expression')
            sys.exit()
    elif not isinstance(x, tokenizer.List):       #constant literal
        return x                
    elif x[0] == BIND:                # (bind var exp)
        (_, var, exp) = x
        if(var==BIND):                #variable name cannot be bind
            print('Invalid Program')
            #raise SyntaxError('Variable name cannot be bind')
            sys.exit()
        env[var] = eval(exp, env)
        if(f):
            return env[var]
    else:                          
        proc = eval(x[0], env)
        args = [eval(exp, env) for exp in x[1:]]
        if(len(args)==1):
            print('Invalid Program')
            #raise SyntaxError('Got less arguments than expected')
            sys.exit()
        try:
            val = args[0]
            for i in range(len(args)-1):  #handling (+ 1 2 3 4)
                val = proc(val, args[i+1])
            return val
        except:
            print('Invalid Program')
            sys.exit()

def helper(lines):
    length = len(lines)
    for i in range(0, length-1):    
        eval(tokenizer.parse(lines[i]))         #evaluating

    #print the answer
    if BIND in lines[length-1]:
        return eval(tokenizer.parse(lines[length-1]),1)
    else:
        return eval(tokenizer.parse(lines[length-1]))
        
if __name__=='__main__':
    filename = raw_input("Enter the path to the file ")
    lines = [line.strip() for line in open(filename, 'r')]
    print helper(lines)
    
